import{ Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class DataService {

  constructor(public http:Http) { }

  getJokes(){
    return this.http.get('https://api.chucknorris.io/jokes/random')
      .map(result => result.json());
  }

  getCategories(){
    return this.http.get('https://api.chucknorris.io/jokes/categories')
      .map(result => result.json());
  }

  getJokeByCategory(category){
    return this.http.get('https://api.chucknorris.io/jokes/random?category=' + category)
      .map(result => result.json());
  }

  getJokeBySearch(search){
    return this.http.get('https://api.chucknorris.io/jokes/search?query=' + search)
      .map(result => result.json());
  }

}
