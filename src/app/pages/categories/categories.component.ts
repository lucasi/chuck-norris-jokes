import { HomeComponent } from './../home/home.component';
import { Joke } from './../../joke.interface';
import { DataService } from './../../services/data.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css',
  '../home/home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CategoriesComponent implements OnInit {

  categories: string[];
  jokeOfCategory: Joke[] = []

  constructor( private data:DataService ) { }

  ngOnInit() {
    this.getCategories();
  }

  getCategories(){
    this.data.getCategories().subscribe((cat) =>{
      this.categories = cat;
    })
  }

  getJokesFromCategory(category:string){
    this.data.getJokeByCategory(category).subscribe((joke) =>{
      this.jokeOfCategory.unshift(joke);
    })
  }

}
