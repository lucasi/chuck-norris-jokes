import { DataService } from './../../services/data.service';
import { Joke } from './../../joke.interface';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css',
    '../home/home.component.css'

  ],
  encapsulation: ViewEncapsulation.None
})
export class SearchComponent implements OnInit {

  searchResult: Joke [] = [];

  constructor(private data:DataService) { }

  ngOnInit() {
  }

  getJokeFromSearch(str:string){
    this.searchResult = null;
    document.getElementById("searchBar").style.backgroundImage="url('../../../assets/searchiconBlack.png')";
    if (str != ""){
      this.data.getJokeBySearch(str).subscribe((search) =>{
        if(search.result == null){
          this.searchResult =[{
            icon_url: "",
            id: "",
            url:"",
            value:"Chuck Norris couldn't find anything like this. Search again"}];
          //console.log(this.searchResult);
        }else
          this.searchResult = search.result;
      })
    }else
      this.searchResult =[{
        icon_url: "",
        id: "",
        url:"",
        value:"Chuck Norris couldn't find anything like this. Search again"}];
  }

  changeColor(){
    document.getElementById("searchBar").style.backgroundImage="url('../../../assets/searchiconGrey.png')";
  }

  deleteJoke(joke){
    for(let i=0; i<this.searchResult.length;i++){
      if(this.searchResult[i] == joke)
        this.searchResult.splice(i,1);
    }

  }

}
