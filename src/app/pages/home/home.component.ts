import { DataService } from './../../services/data.service';
import { Joke } from './../../joke.interface';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  jokes:Joke [] = [];

  constructor(private data:DataService) { }

  ngOnInit() {
    this.getJoke();
  }

  getJoke(){
    this.data.getJokes().subscribe((joke) => {
      this.jokes.unshift(joke);
    });
  }

}
