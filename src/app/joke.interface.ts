export interface Joke{
    categories?: string[],
    icon_url: string,
    id: string;
    url:string,
    value: string
}